import React, { useState } from 'react'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'

import { useLiveQuery } from 'dexie-react-hooks'
import db from './database'

import CategoryAdd from './page/CategoryAdd'
import CategoryEdit from './page/CategoryEdit'
import CategoryIndex from './page/CategoryIndex'
import Dashboard from './page/Dashboard'
import Navigation from './component/Navigation'
import ItemAdd from './page/ItemAdd'
import ItemIndex from './page/ItemIndex'
import ItemEdit from './page/ItemEdit'

function App () {
  const categories = useLiveQuery(() => db.categories.orderBy('name').toArray())
  const items = useLiveQuery(() => db.items.orderBy('date').toArray())
  const [selectedCategory, setSelectedCategory] = useState()

  if (!categories || !items) {
    return null
  }

  if (!selectedCategory && categories.length > 0) {
    setSelectedCategory(categories[0].uuid)
  }

  return (
    <Router>
      <Navigation/>
      <div className="container mt-4">
        <Switch>
          <Route exact path="/">
            <Dashboard
              categories={categories}
              items={items}
              selectedCategory={selectedCategory}
              setSelectedCategory={setSelectedCategory}
            />
          </Route>
          <Route exact path="/category">
            <CategoryIndex/>
          </Route>
          <Route exact path="/category/add">
            <CategoryAdd/>
          </Route>
          <Route exact path="/category/edit/:uuid">
            <CategoryEdit/>
          </Route>
          <Route exact path="/item">
            <ItemIndex/>
          </Route>
          <Route exact path="/item/add">
            <ItemAdd setSelectedCategory={setSelectedCategory}/>
          </Route>
          <Route exact path="/item/edit/:uuid">
            <ItemEdit/>
          </Route>
        </Switch>
      </div>
    </Router>
  )
}

export default App
