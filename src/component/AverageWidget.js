import React from 'react'
import PropTypes from 'prop-types'

export default function AverageWidget ({ items, unit }) {
  if (items.length === 0) {
    return null
  }

  let sum = 0
  for (const item of items) {
    sum += item.value
  }
  const average = sum / items.length

  return (
    <div className="text-center">
      <big>{average.toFixed(2)} {unit}</big><br/>
      <small className="text-muted">{items.length} values</small>
    </div>
  )
}

AverageWidget.propTypes = {
  items: PropTypes.arrayOf(PropTypes.shape({
    date: PropTypes.string.isRequired,
    value: PropTypes.number.isRequired
  })).isRequired,
  unit: PropTypes.string.isRequired
}
