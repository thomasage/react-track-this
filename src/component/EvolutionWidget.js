import React from 'react'
import PropTypes from 'prop-types'
import Highcharts from 'highcharts'
import HighchartsReact from 'highcharts-react-official'

export default function EvolutionWidget ({ items, unit }) {
  if (items.length === 0) {
    return null
  }

  const options = {
    chart: {
      type: 'spline'
    },
    legend: {
      enabled: false
    },
    plotOptions: {
      spline: {
        dataLabels: {
          enabled: true
        }
      }
    },
    series: [
      {
        data: []
      }
    ],
    title: false,
    xAxis: {
      plotBands: [],
      type: 'datetime'
    },
    yAxis: {
      title: {
        text: unit
      }
    }
  }

  for (const item of items) {
    const date = new Date(item.date + 'T12:00:00Z')
    options.series[0].data.push([date.valueOf(), item.value])
  }

  const dateStart = new Date(items[0].date + 'T12:00:00Z')
  const dateStop = new Date(items[items.length - 1].date + 'T12:00:00Z')
  let day = new Date(dateStart)
  while (day <= dateStop) {
    if (day.getDay() === 0 || day.getDay() === 6) {
      options.xAxis.plotBands.push({
        color: '#eee',
        from: new Date(day.toISOString().substring(0, 11) + '00:00:00Z'),
        to: new Date(day.toISOString().substring(0, 11) + '23:59:59Z')
      })
    }
    day = new Date(day.setDate(day.getDate() + 1))
  }

  return (
    <HighchartsReact
      highcharts={Highcharts}
      options={options}
    />
  )
}

EvolutionWidget.propTypes = {
  items: PropTypes.arrayOf(PropTypes.shape({
    date: PropTypes.string.isRequired,
    value: PropTypes.number.isRequired
  })).isRequired,
  unit: PropTypes.string.isRequired
}
