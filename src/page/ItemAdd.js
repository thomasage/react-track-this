import React from 'react'
import PropTypes from 'prop-types'
import { Link, useHistory } from 'react-router-dom'
import { useLiveQuery } from 'dexie-react-hooks'
import { categoryGetAll, itemAdd } from '../database'
import ItemForm from '../component/ItemForm'

export default function ItemAdd ({ setSelectedCategory }) {
  const categories = useLiveQuery(() => categoryGetAll())
  const history = useHistory()

  if (!categories || categories.length === 0) {
    return null
  }

  let defaultCategory = categories[0].uuid
  const urlParams = new URLSearchParams(location.search)
  if (urlParams.has('category')) {
    defaultCategory = urlParams.get('category')
  }

  function onNewItem ({ category, date, value }) {
    itemAdd({ category, date, value })
      .then(() => { setSelectedCategory(category) })
      .then(() => { history.push('/') })
  }

  return (
    <div>
      <div className="d-flex flex-row align-items-center">
        <h1 className="flex-grow-1">Add an item</h1>
        <Link to="/item" className="btn btn-outline-secondary">Back</Link>
      </div>
      <ItemForm
        defaultCategory={defaultCategory}
        defaultDate={new Date().toISOString().substring(0, 10)}
        onSubmit={onNewItem}
      />
    </div>
  )
}

ItemAdd.propTypes = {
  setSelectedCategory: PropTypes.func.isRequired
}
