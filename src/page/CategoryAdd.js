import React, { useState } from 'react'
import CategoryForm from '../component/CategoryForm'
import db from '../database'
import { v4 as uuidv4 } from 'uuid'
import { Link, Redirect } from 'react-router-dom'

export default function CategoryAdd () {
  const [redirect, setRedirect] = useState(false)

  function onNewCategory ({ name, unit }) {
    db.categories.add({
      name,
      unit,
      uuid: uuidv4()
    })
    setRedirect(true)
  }

  if (redirect) {
    return <Redirect to="/category"/>
  }

  return (
    <div>
      <div className="d-flex flex-row align-items-center">
        <h1 className="flex-grow-1">Add a category</h1>
        <Link to="/category" className="btn btn-outline-secondary">Back</Link>
      </div>
      <CategoryForm onSubmit={onNewCategory}/>
    </div>
  )
}
