import React from 'react'
import PropTypes from 'prop-types'
import EvolutionWidget from './EvolutionWidget'
import MinWidget from './MinWidget'
import MaxWidget from './MaxWidget'
import AverageWidget from './AverageWidget'

export default function AllWidgets ({ items, unit }) {
  return (
    <div>
      <EvolutionWidget
        items={items}
        unit={unit}
      />
      <div className="row">
        <div className="col-sm-4 mt-4">
          <div className="card">
            <div className="card-header">Min</div>
            <div className="card-body">
              <MinWidget
                items={items}
                unit={unit}
              />
            </div>
          </div>
        </div>
        <div className="col-sm-4 mt-4">
          <div className="card">
            <div className="card-header">Max</div>
            <div className="card-body">
              <MaxWidget
                items={items}
                unit={unit}
              />
            </div>
          </div>
        </div>
        <div className="col-sm-4 mt-4">
          <div className="card">
            <div className="card-header">Average</div>
            <div className="card-body">
              <AverageWidget
                items={items}
                unit={unit}
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

AllWidgets.propTypes = {
  items: PropTypes.arrayOf(PropTypes.shape({
    date: PropTypes.string.isRequired,
    value: PropTypes.number.isRequired
  })).isRequired,
  unit: PropTypes.string.isRequired
}
