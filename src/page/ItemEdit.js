import React, { useState } from 'react'
import { Link, Redirect, useParams } from 'react-router-dom'
import db from '../database'
import { useLiveQuery } from 'dexie-react-hooks'
import ItemForm from '../component/ItemForm'

export default function ItemEdit () {
  const { uuid } = useParams()
  const item = useLiveQuery(() => db.items.get(uuid))
  const [redirect, setRedirect] = useState(false)
  if (!item) {
    return null
  }

  function onItemDelete () {
    db.items.delete(uuid)
    setRedirect(true)
  }

  function onItemUpdate ({ category, date, value }) {
    db.items.update(uuid, { category, date, value })
    setRedirect(true)
  }

  if (redirect) {
    return <Redirect to="/item"/>
  }

  return (
    <div>
      <div className="d-flex flex-row align-items-center">
        <h1 className="flex-grow-1">Edit a category</h1>
        <Link to="/category" className="btn btn-outline-secondary">
          <i className="fas fa-list"/> Back
        </Link>
      </div>
      <ItemForm
        defaultCategory={item.category}
        defaultDate={item.date}
        defaultValue={item.value}
        onSubmit={onItemUpdate}
      />
      <button type="submit" className="btn btn-danger mt-5" onClick={onItemDelete}>
        <i className="fas fa-trash"/> Delete
      </button>
    </div>
  )
}
