import React from 'react'
import { Link } from 'react-router-dom'
import { useLiveQuery } from 'dexie-react-hooks'
import db from '../database'

export default function ItemIndex () {
  const categories = useLiveQuery(() => db.categories.toArray())
  const items = useLiveQuery(() => db.items.orderBy('date').reverse().toArray())

  if (!categories || !items) {
    return null
  }

  function getCategoryName (categoryUuid) {
    const category = categories.find(category => category.uuid === categoryUuid)
    return category ? category.name : ''
  }

  function getCategoryUnit (categoryUuid) {
    const category = categories.find(category => category.uuid === categoryUuid)
    return category ? category.unit : ''
  }

  return (
    <div>
      <div className="d-flex flex-row align-items-center">
        <h1 className="flex-grow-1">Items</h1>
        {categories.length === 0 || <Link to="/item/add" className="btn btn-outline-primary">Add</Link>}
      </div>
      <table className="table mt-4">
        <thead>
        <tr>
          <th>Date</th>
          <th>Value</th>
          <th>Category</th>
          <th/>
        </tr>
        </thead>
        <tbody>
        {items.map(item =>
          <tr key={item.uuid}>
            <td>{item.date}</td>
            <td>{item.value} {getCategoryUnit(item.category)}</td>
            <td>{getCategoryName(item.category)}</td>
            <td>
              <Link to={`/item/edit/${item.uuid}`} className="btn btn-outline-primary">
                Edit
              </Link>
            </td>
          </tr>
        )}
        {items.length > 0 || (
          <tr>
            <td colSpan="4" className="p-5 text-center text-muted">No items</td>
          </tr>
        )}
        </tbody>
      </table>
    </div>
  )
}
