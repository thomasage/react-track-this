import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'

export default function OnBoardingItem ({ category }) {
  return (
    <div className="mt-5 text-center">
      <Link to={`/item/add?category=${category}`}
            className="btn btn-outline-primary">
        Add a item to start
      </Link>
    </div>
  )
}

OnBoardingItem.propTypes = {
  category: PropTypes.string.isRequired
}
