import React from 'react'
import PropTypes from 'prop-types'

export default function CategoryList ({ categories }) {
  return (
    <div>
      {categories.map(category =>
        <div key={category.uuid}>
          {category.name} <small>{category.unit}</small>
        </div>
      )}
      {categories.length > 0 || <div>No categories</div>}
    </div>
  )
}

CategoryList.propTypes = {
  categories: PropTypes.arrayOf(PropTypes.shape({
    name: PropTypes.string.isRequired,
    unit: PropTypes.string.isRequired,
    uuid: PropTypes.string.isRequired
  })).isRequired
}
