import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
import AllWidgets from '../component/AllWidgets'
import OnBoardingCategory from '../component/OnBoardingCategory'
import OnBoardingItem from '../component/OnBoardingItem'

export default function Dashboard ({ categories, items, selectedCategory, setSelectedCategory }) {
  if (categories.length === 0) {
    return <OnBoardingCategory/>
  }

  const filteredItems = items.filter(item => item.category === selectedCategory)

  /**
   * @param {string} uuid
   * @returns {string}
   */
  function getUnit (uuid) {
    const category = categories.find(category => category.uuid === uuid)
    return category ? category.unit : ''
  }

  return (
    <div>
      <div className="mb-4 mt-4 d-flex flex-row">
        <select className="form-control"
                value={selectedCategory}
                onChange={event => setSelectedCategory(event.target.value)}>
          {categories.map(category =>
            <option key={category.uuid} value={category.uuid}>{category.name}</option>
          )}
        </select>
        <Link to={`/item/add?category=${selectedCategory}`}
              className="btn btn-outline-primary ms-3">
          Add
        </Link>
      </div>
      {filteredItems.length === 0 && (
        <OnBoardingItem category={selectedCategory}/>
      )}
      {filteredItems.length > 0 && (
        <AllWidgets
          items={filteredItems}
          unit={getUnit(selectedCategory)}
        />
      )}
    </div>
  )
}

Dashboard.defaultProps = {
  selectedCategory: ''
}
Dashboard.propTypes = {
  categories: PropTypes.array.isRequired,
  items: PropTypes.array.isRequired,
  selectedCategory: PropTypes.string,
  setSelectedCategory: PropTypes.func.isRequired
}
