import Dexie from 'dexie'
import { v4 as uuidv4 } from 'uuid'

const db = new Dexie('track_this')
db.version(4).stores({
  categories: 'uuid,name',
  items: 'uuid,date,category,value'
})

/**
 * @returns {Promise}
 */
export function categoryGetAll () {
  return db.categories.orderBy(name).toArray()
}

/**
 * @param {string} category
 * @param {string} date
 * @param {number} value
 * @returns {Promise}
 */
export function itemAdd ({ category, date, value }) {
  return db.items.add({
    category,
    date,
    uuid: uuidv4(),
    value
  })
}

export default db
