import React from 'react'
import { Link } from 'react-router-dom'

export default function OnBoardingCategory () {
  return (
    <div className="mt-5 text-center">
      <Link to="/category/add" className="btn btn-outline-primary">
        Add a category to start
      </Link>
    </div>
  )
}
