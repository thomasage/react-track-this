import React, { useState } from 'react'
import PropTypes from 'prop-types'
import { useLiveQuery } from 'dexie-react-hooks'
import db from '../database'

export default function ItemForm ({
  defaultCategory = '',
  defaultDate = '',
  defaultValue = null,
  onSubmit
}) {
  const categories = useLiveQuery(() => db.categories.toArray())
  const [newItem, setNewItem] = useState({
    category: defaultCategory,
    date: defaultDate,
    value: defaultValue
  })

  if (!categories) {
    return null
  }

  function handleSubmit (event) {
    event.preventDefault()
    onSubmit(newItem)
    setNewItem({
      ...newItem,
      date: defaultDate,
      value: defaultValue
    })
  }

  return (
    <form onSubmit={handleSubmit}>
      <div className="mt-3 row align-items-center">
        <label htmlFor="item_date" className="form-label col-sm-2">Date</label>
        <div className="col-sm-10">
          <input
            type="date"
            id="item_date"
            className="form-control"
            required
            value={newItem.date}
            onChange={event => setNewItem({ ...newItem, date: event.target.value })}
          />
        </div>
      </div>
      <div className="mt-3 row align-items-center">
        <label htmlFor="item_value" className="form-label col-sm-2">Value</label>
        <div className="col-sm-10">
          <input
            type="number"
            id="item_value"
            className="form-control"
            step="0.01"
            min="0"
            autoFocus
            required
            value={newItem.value ?? ''}
            onChange={event => setNewItem({
              ...newItem,
              value: event.target.value ? parseFloat(event.target.value) : ''
            })}
          />
        </div>
      </div>
      <div className="mt-3 row align-items-center">
        <label htmlFor="item_category" className="form-label col-sm-2">Category</label>
        <div className="col-sm-10">
          <select id="item_category"
                  className="form-control"
                  required
                  value={newItem.category}
                  onChange={event => setNewItem({ ...newItem, category: event.target.value })}>
            {categories.map(category =>
              <option key={category.uuid} value={category.uuid}>
                {category.name} [{category.unit}]
              </option>
            )}
          </select>
        </div>
      </div>
      <div className="mt-3">
        <button type="submit" className="btn btn-success">
          Save
        </button>
      </div>
    </form>
  )
}

ItemForm.defaultProps = {
  defaultCategory: '',
  defaultDate: '',
  defaultValue: null
}
ItemForm.propTypes = {
  defaultCategory: PropTypes.string,
  defaultDate: PropTypes.string,
  defaultValue: PropTypes.number,
  onSubmit: PropTypes.func.isRequired
}
