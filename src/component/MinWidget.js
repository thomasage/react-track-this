import React from 'react'
import PropTypes from 'prop-types'

export default function MinWidget ({ items, unit }) {
  if (items.length === 0) {
    return null
  }

  let min = null
  for (const item of items) {
    if (min === null || item.value < min.value) {
      min = item
    }
  }

  return (
    <div className="text-center">
      <big>{min.value} {unit}</big><br/>
      <small className="text-muted">{new Date(min.date).toLocaleDateString()}</small>
    </div>
  )
}

MinWidget.propTypes = {
  items: PropTypes.arrayOf(PropTypes.shape({
    date: PropTypes.string.isRequired,
    value: PropTypes.number.isRequired
  })).isRequired,
  unit: PropTypes.string.isRequired
}
