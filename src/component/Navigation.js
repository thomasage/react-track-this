import React from 'react'
import { Link, useLocation } from 'react-router-dom'

export default function Navigation () {
  const location = useLocation()

  return (
    <nav className="navbar navbar-expand-lg navbar-light bg-light">
      <div className="container">
        <a href="/" className="navbar-brand">Track This!</a>
        <button className="navbar-toggler"
                type="button"
                data-bs-toggle="collapse"
                data-bs-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent"
                aria-expanded="false"
                aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"/>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item">
              <Link to="/" className={`nav-link ${location.pathname === '/' ? 'active' : ''}`}>Dashboard</Link>
            </li>
            <li className="nav-item">
              <Link to="/item" className={`nav-link ${location.pathname.startsWith('/item') ? 'active' : ''}`}>
                Items
              </Link>
            </li>
            <li className="nav-item">
              <Link to="/category" className={`nav-link ${location.pathname.startsWith('/category') ? 'active' : ''}`}>
                Categories
              </Link>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  )
}
