import React, { useState } from 'react'
import { Link, Redirect, useParams } from 'react-router-dom'
import db from '../database'
import { useLiveQuery } from 'dexie-react-hooks'
import CategoryForm from '../component/CategoryForm'

export default function CategoryEdit () {
  const { uuid } = useParams()
  const category = useLiveQuery(() => db.categories.get(uuid))
  const [redirect, setRedirect] = useState(false)
  if (!category) {
    return null
  }

  function onCategoryDelete () {
    db.categories.delete(uuid)
    setRedirect(true)
  }

  function onCategoryUpdate ({ name, unit }) {
    db.categories.update(uuid, { name, unit })
    setRedirect(true)
  }

  if (redirect) {
    return <Redirect to="/category"/>
  }

  return (
    <div>
      <div className="d-flex flex-row align-items-center">
        <h1 className="flex-grow-1">Edit a category</h1>
        <Link to="/category" className="btn btn-outline-secondary">
          <i className="fas fa-list"/> Back
        </Link>
      </div>
      <CategoryForm
        defaultName={category.name}
        defaultUnit={category.unit}
        onSubmit={onCategoryUpdate}
      />
      <button type="submit" className="btn btn-danger mt-5" onClick={onCategoryDelete}>
        <i className="fas fa-trash"/> Delete
      </button>
    </div>
  )
}
