import React from 'react'
import PropTypes from 'prop-types'

export default function ItemList ({ categories, items }) {
  function getCategoryName (uuid) {
    if (!uuid) {
      return ''
    }
    const category = categories.find(category => category.uuid === uuid)
    if (!category) {
      return ''
    }
    return category.name
  }

  return (
    <div>
      {items.map(item =>
        <div key={item.uuid}>
          {item.date} = {item.value} [{getCategoryName(item.category)}]
        </div>
      )}
      {items.length > 0 || <div>No items</div>}
    </div>
  )
}

ItemList.propTypes = {
  categories: PropTypes.arrayOf(PropTypes.shape({
    name: PropTypes.string.isRequired,
    unit: PropTypes.string.isRequired,
    uuid: PropTypes.string.isRequired
  })).isRequired,
  items: PropTypes.arrayOf(PropTypes.shape({
    category: PropTypes.string.isRequired,
    date: PropTypes.string.isRequired,
    uuid: PropTypes.string.isRequired,
    value: PropTypes.number.isRequired
  })).isRequired
}
