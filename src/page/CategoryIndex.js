import React from 'react'
import { Link } from 'react-router-dom'
import { useLiveQuery } from 'dexie-react-hooks'
import db from '../database'

export default function CategoryIndex () {
  const categories = useLiveQuery(() => db.categories.orderBy('name').toArray())
  const items = useLiveQuery(() => db.items.toArray())

  if (!categories || !items) {
    return null
  }

  function getItems (category) {
    return items.filter(item => item.category === category).length
  }

  return (
    <div>
      <div className="d-flex flex-row align-items-center">
        <h1 className="flex-grow-1">Categories</h1>
        <Link to="/category/add" className="btn btn-outline-primary">Add</Link>
      </div>
      <table className="table mt-4">
        <thead>
        <tr>
          <th>Name</th>
          <th>Unit</th>
          <th>Items</th>
          <th/>
        </tr>
        </thead>
        <tbody>
        {categories.map(category =>
          <tr key={category.uuid}>
            <td>{category.name}</td>
            <td>{category.unit}</td>
            <td>{getItems(category.uuid)}</td>
            <td>
              <Link to={`/category/edit/${category.uuid}`} className="btn btn-outline-primary">
                Edit
              </Link>
            </td>
          </tr>
        )}
        {categories.length > 0 || (
          <tr>
            <td colSpan="4" className="p-5 text-center text-muted">No categories</td>
          </tr>
        )}
        </tbody>
      </table>
    </div>
  )
}
