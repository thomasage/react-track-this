# Track this!

A simple app to track data evolution

## Development

* Run `yarn start`
* Open [http://localhost:3000](http://localhost:3000) to view it in the browser.
* The page will reload if you make edits. You will also see any lint errors in the console.

## Build

* Run `yarn build`
* Builds the app for production to the `build` folder.
