import React from 'react'
import PropTypes from 'prop-types'

export default function MaxWidget ({ items, unit }) {
  if (items.length === 0) {
    return null
  }

  let max = null
  for (const item of items) {
    if (max === null || item.value > max.value) {
      max = item
    }
  }

  return (
    <div className="text-center">
      <big>{max.value} {unit}</big><br/>
      <small className="text-muted">{new Date(max.date).toLocaleDateString()}</small>
    </div>
  )
}

MaxWidget.propTypes = {
  items: PropTypes.arrayOf(PropTypes.shape({
    date: PropTypes.string.isRequired,
    value: PropTypes.number.isRequired
  })).isRequired,
  unit: PropTypes.string.isRequired
}
