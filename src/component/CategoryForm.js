import React, { useState } from 'react'
import PropTypes from 'prop-types'

export default function CategoryForm ({ defaultName = '', defaultUnit = '', onSubmit }) {
  const [newCategory, setNewCategory] = useState({
    name: defaultName,
    unit: defaultUnit
  })

  function handleSubmit (event) {
    event.preventDefault()
    onSubmit(newCategory)
    setNewCategory({
      name: defaultName,
      unit: defaultUnit
    })
  }

  return (
    <form onSubmit={handleSubmit}>
      <div className="row mt-3 align-items-center">
        <label htmlFor="category_name" className="form-label col-sm-2">Name</label>
        <div className="col-sm-10">
          <input
            type="text"
            id="category_name"
            className="form-control"
            autoFocus
            value={newCategory.name}
            onChange={event => setNewCategory({ ...newCategory, name: event.target.value })}
          />
        </div>
      </div>
      <div className="mt-3 row align-items-center">
        <label htmlFor="category_unit" className="form-label col-sm-2">Unit</label>
        <div className="col-sm-10">
          <input
            type="text"
            id="category_unit"
            className="form-control"
            value={newCategory.unit}
            onChange={event => setNewCategory({ ...newCategory, unit: event.target.value })}
          />
        </div>
      </div>
      <div className="mt-3">
        <button type="submit" className="btn btn-success">Save</button>
      </div>
    </form>
  )
}

CategoryForm.defaultProps = {
  defaultName: '',
  defaultUnit: ''
}
CategoryForm.propTypes = {
  defaultName: PropTypes.string,
  defaultUnit: PropTypes.string,
  onSubmit: PropTypes.func.isRequired
}
